const showDescription = function (event) {
  const activeTab = document.querySelector(".active");
  activeTab.classList.remove("active");

  const currentItem = event.target.closest(".tabs-title");
  currentItem.classList.add("active");
  const dataName = currentItem.dataset.name;
  const tabs = document.querySelectorAll(".tabs-content > li");
  currentDescription = document.querySelector(`li.${dataName}`);
  tabs.forEach((element) => {
    element.hidden = true;
    if (element.classList.contains(dataName)) element.hidden = false;
  });
};
document.querySelector(".tabs").addEventListener("click", showDescription);
